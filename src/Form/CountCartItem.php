<?php  

namespace Drupal\count_cart_item\Form;  
use Drupal\Core\Form\ConfigFormBase;  
use Drupal\Core\Form\FormStateInterface;  
use Drupal\Core\Form\FormBase;

class CountCartItem extends FormBase {
   
	/**  
	* {@inheritdoc}  
	*/  
	public function getFormId() {
		return 'count_cart_item';
	}
	
	/**  
	* {@inheritdoc}  
	*/  
	public function buildForm(array $form, FormStateInterface $form_state) {  
		$form = parent::buildForm($form, $form_state);
		$form['#theme'] = 'count-cart-item';
		
		return $form;
	}  

	public function submitForm(array &$form, FormStateInterface $form_state) {
	}
}  