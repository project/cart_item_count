<?php

namespace Drupal\count_cart_item\Plugin\Block;

use Drupal\Core\Access\AccessResult;
use Drupal\Core\Block\BlockBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\views\Views;
use Drupal\user\Entity\User;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Database\Connection;
use Drupal\commerce_order\Entity\Order;


/**
* Provides a block with a simple text.
*
* @Block(
*   id = "count_cart_item_block",
*   admin_label = @Translation("Cart Item Count Block"),
* )
*/
class CountCartItem extends BlockBase {

	/**
	* {@inheritdoc}
	*/
	public function build() {

	$query = \Drupal::entityQuery('commerce_order')
	->condition('uid',\Drupal::currentUser()->id())
	->condition('cart', 1)
	->sort('created', 'DESC')
	->range(0,1);
	$cartsOrders = $query->execute();
	if(count($cartsOrders) > 0) {
		$lastCart = current(Order::loadMultiple($cartsOrders));
		$cartItemsCount = count($lastCart->getItems());
		} else {
		  $cartItemsCount = 0;
		}
		return [
		  '#theme' => 'count-cart-item',
		  '#content' =>  $cartItemsCount
		];
	}

	/**
	* {@inheritdoc}
	*/
	protected function blockAccess(AccountInterface $account) {
		return AccessResult::allowedIfHasPermission($account, 'access content');
	} 
}