-- SUMMARY --

Cart Item Count module allows us to get current product item count which are available into my shopping cart.

This module will work with Drupal Commerce.

you need to enable this module and configure steps which are mentioned below.

For a full description of the module, visit the project page:
  https://www.drupal.org/project/cart_item_count

-- REQUIREMENTS --

None.

-- INSTALLATION --

* Install as usual, see http://drupal.org/node/895232 for further information.

-- CONFIGURATION --

* Enable this module same as other drupal modules (Drupal Commerce must be enabled)
* Go in Structure>blocks
* Click on "Place Block" button in which region you want to show up
* Place "Cart Item Count Block" in that region
* Configure your display settings and save it.

-- CONTACT --

Current maintainers:
* Vishal D. Sheladiya (vishalsheladiya) - http://drupal.org/user/2915771